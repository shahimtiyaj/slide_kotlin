package com.netizen.slide

/**
 * Created by Parsania Hardik on 03-Jan-17.
 */
class ImageModel {

    private var image_drawable: Int = 0
    private var txt1: String? = null
    private var txt2: String? = null

    fun getImage_drawables(): Int {
        return image_drawable
    }

    fun setImage_drawables(image_drawable: Int) {
        this.image_drawable = image_drawable
    }


    fun getTxt1(): String? {
        return txt1
    }

    fun setTxt1(txt1: String?) {
        this.txt1 = txt1
    }

    fun getTxt2(): String? {
        return txt2
    }

    fun setTxt2(txt2: String?) {
        this.txt2 = txt2
    }
}